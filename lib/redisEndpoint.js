/**
 * Created by noamtz on 9/17/15.
 */
var AWS = require('aws-sdk');
var dns = require('dns');
var redisClient = require('redis').createClient;
var url = require('url');

var internals = {
    elasticache: undefined,
    redisEndpoint: undefined,
    serverBroadcast: undefined,
    config: process.env.CONFIG_FILE ? require(process.env.CONFIG_FILE) : undefined
};

internals.init = function(next) {
    if(process.env.NODE_ENV == 'dev' || process.env.NODE_ENV == 'production') {
        console.log('Preparing redis-connections...');

        if (process.env.REDISCLOUD_URL) {

            var redisURL = url.parse(process.env.REDISCLOUD_URL);

            internals.pub = redisClient(redisURL.port, redisURL.hostname, { auth_pass: redisURL.auth.split(":")[1] });
            internals.sub = redisClient(redisURL.port, redisURL.hostname, { detect_buffers: true, auth_pass: redisURL.auth.split(":")[1] });
        } else {

            internals.pub = redisClient(6379 , 'localhost', { auth_pass: null });//
            internals.sub = redisClient(6379 , 'localhost', { detect_buffers: true, auth_pass: null });
        }


        internals.pub.on('ready', function() {
            console.log('PUBLISH is ready');
            internals.sub.on('ready', function() {
                console.log('SUBSCRIBE is ready');
                next(null, {
                    pub: internals.pub,
                    sub: internals.sub
                });
            });
        });

    } else {
        AWS.config.update({region: internals.config.Region});
        internals.elasticache = new AWS.ElastiCache();
        internals.getRedisEndpoint(next);
    }
};

internals.getRedisEndpoint = function (next) {
    internals.elasticache.describeCacheClusters({ CacheClusterId: internals.config.ElastiCache, ShowCacheNodeInfo:true},
        function(err, data) {
            if(err) {
                console.log('Error describing Cache Cluster:'+err);
                return next(err);
            }

            console.log('Describe Cache Cluster Id:'+ internals.config.ElastiCache);
            if (data.CacheClusters[0].CacheClusterStatus == 'available') {
                internals.redisEndpoint = data.CacheClusters[0].CacheNodes[0].Endpoint;
                internals.configureRedisStore(next);
            } else {
                process.nextTick(internals.getRedisEndpoint); // Try again until available
            }
        });
};

internals.configureRedisStore = function(next) {
    dns.lookup(internals.redisEndpoint.Address, function (err, address) {
        // Sometimes the DNS propagation can be slow, try again after 1 second
        if (err) {
            setTimeout(internals.configureRedisStore, 1000);
        } else {
            internals.pub    = redisClient(internals.redisEndpoint.Port, address);
            internals.sub    = redisClient(internals.redisEndpoint.Port, address, { detect_buffers: true });

            internals.pub.on('ready', function() {
                internals.sub.on('ready', function() {
                    next(null, {
                        pub: internals.pub,
                        sub: internals.sub
                    });
                });
            })
        }
    });
};


module.exports = {
    init: internals.init
};